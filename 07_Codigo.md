# Código en linea

En JavaScript se utiliza `var`, `let`, `const` para declarar variables. 

# Código en bloque 

Bloque de código para JavaScript.

```javascript
const mensaje = 'Hola mundo';
alert(`${mensaje}`);
```