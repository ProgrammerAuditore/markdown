# Parrafo dinamicas

![Hola][ImagenExterno]

blabla blabla blabla blabla blabla blabla blabla
blabla blabla blabla [Ver post][LinkExterno].

![Imagen interno][ImagenInterno] <br>
blabla blabla blabla blabla blabla blabla blabla
blabla blabla blabla [Ver post][LinkInterno].


<!-- Enlaces dinamicos -->
[ImagenExterno]: https://media-exp1.licdn.com/dms/image/C4E22AQFMQphugG1xhA/feedshare-shrink_800/0/1640603040984?e=1643846400&v=beta&t=ZeuYUSEldOz2Y5sBrPrWj3BVSa-4-qZrkKswB-2FRyA
[LinkExterno]: https://www.linkedin.com/posts/adrian-duran-gomez_ternarias-anidadas-en-javascript-entre-el-activity-6881639137041035264-qy1
[ImagenInterno]: img/UserMe.jpg
[LinkInterno]: img/UserMe.jpg