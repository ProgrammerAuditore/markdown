# Imagenes externas

![](https://media-exp1.licdn.com/dms/image/C5622AQF4KzTiabhp7A/feedshare-shrink_800/0/1640527203760?e=1643846400&v=beta&t=n_QpPPkcWzv13Uzms-eBVuelbzp2bNbsKMYLg0FvwOE)

# Imagenes internas

![Foto de perfil](img/UserMe.jpg "Mi foto de perfil")

# Multiples imagenes

![imagen 1][1]

bla bla bla bla bla bla bla bla.

![imagen 2][2].

bla bla bla bla bla bla bla bla.

[1]: img/UserMe.jpg
[2]: img/UserMe.jpg