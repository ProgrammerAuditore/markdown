# Cita

Esto es una linea normal

> Esto es una cita.
> La cita sigue [...]
>
> > Esto es una cita anidada...
> > aquí sigue la cita anidada.
>
> [...] Esto forma parte de la cita de primer nivel.
 

# Cita con listas

> Cita con listas
> - C
> - C++
> - C#